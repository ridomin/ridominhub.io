---
layout:	post
title: Why the web is not enough
subtitle: native development for Desktop
author:	rido
header-img: img/cascade2.jpg
---

# Why the web is not enough

Since we started with the web 20 years ago, we always believe at some point 
browsers will replace the OS, and will offer the platform to build apps.

Although in theory this is still true, the reality showed us that the utopy will work
for some players but never will replace native development.
