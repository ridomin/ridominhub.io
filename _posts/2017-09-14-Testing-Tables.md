---
layout: post
title: Testing Tables in Markdown
author: rido
header-img: "img/cascade2.jpg"
---

This is sample table from MD

|  |Stage 1|Stage 2|Stage 3|
|:------|:-------|:-------|:-------|
|**Authors**|Command Line tools to enable Sign and Verify packages.||Cross platform support.|
|**Servers (NuGet.org)**|Validate Signed Packages on submission.|Generate source checksums.|Configure server policies.|
|**Consumers**|Visual indicator in Visual Studio NuGet Package Manager for signed and verified packages on NuGet.org.<br/> Validate author signatures when obtaining packages (install, restore or update) from CLI or IDE tools. | Validate complete dependency graph for signed packages.<br /> Validate source checksums. <br /> Identify NuGet.org packages in multi-source environments. | Package Source Settings UI to specify client policy by source. <br/>Trusted author dialog UI.|

Same Table with HTML and CSS

<style type="text/css">
table.posttable {		
	border-width: 1px;
	border-color: #004880;
	border-collapse: collapse;    
}
table.posttable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color:white;
	background-color: #004880;    
    color:white;
    text-align: left;
}
table.posttable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #004880;
	background-color: #ffffff;
    vertical-align: top
}
</style>

<table class="posttable" >
    <thead>
        <tr>
            <th width="10%"></th>
            <th width="30%">Stage 1</th>
            <th width="30%">Stage 2</th>
            <th width="30%">Stage 3</th>            
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Authors</b></td>
            <td>Command Line tools to enable Sign and Verify packages.</td>
            <td></td>
            <td>Cross platform support.</td>
        </tr>
        <tr>
            <td><b>Servers (NuGet.org)</b></td>
            <td>Validate Signed Packages on submission.</td>
            <td>Generate source checksums.</td>
            <td>Configure server policies.</td>
        </tr>
        <tr>
            <td><b>Consumers</b></td>
            <td>
                Visual indicator in Visual Studio NuGet Package Manager for signed and verified packages on NuGet.org.
                <p>Validate author signatures when obtaining packages (install, restore or update) from CLI or IDE tools.</p>    
            </td>
            <td>
                Validate complete dependency graph for signed packages.
                <p>Validate source checksums. <br /> Identify NuGet.org packages in multi-source environments.</p>
            </td>
            <td>
                Package Source Settings UI to specify client policy by source.
                <p>
                    Trusted author dialog UI.
                </p>
            </td>
        </tr>  
    </tbody>  
</table>





